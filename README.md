# Description
This repository contains pipeline files to an LTS release process, update LKFT's SQUAD group/projects and submit a tuxplan to tuxsuite for the kernel version that is under review.

## Variables
Variables which require to be setup in CI/CD -> variables are 'SQUAD_TOKEN' and 'TUXSUITE_TOKEN'.
